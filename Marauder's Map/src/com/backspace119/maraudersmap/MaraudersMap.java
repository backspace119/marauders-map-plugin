package com.backspace119.maraudersmap;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.map.MapView;
import org.bukkit.plugin.java.JavaPlugin;

public class MaraudersMap extends JavaPlugin
{
	static final Logger logger = Logger.getLogger("Minecraft");

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		if(cmd.getName().equalsIgnoreCase("gibmap"))
		{
			if(!(sender instanceof Player))
			{
				sender.sendMessage("This command can be used by players only");
				return true;
			}

			byte scale = args.length > 0 ? Byte.parseByte(args[0]) : 1;

			Player player = (Player)sender;
			logger.info("Giving " + player.getDisplayName() + " a Marauder's Map");
			player.getInventory().addItem(MaraudersMap.createMapItem(player, scale));
			return true;
		}

		return false;
	}

	private static ItemStack createMapItem(Player player, byte scale)
	{
		if(scale < 0)
		{
			scale = 0;
		}
		else if(scale > 2)
		{
			scale = 2;
		}

		// TODO: make other scales show more (valid scales = 0 to 4)

		MapView map = Bukkit.createMap(player.getWorld());
		map.addRenderer(new MaraudersMapRenderer());

		// set a larger region as explored so that it is visible
		map.setScale(MapView.Scale.valueOf((byte)2));
		map.setScale(MapView.Scale.valueOf(scale));

		ItemStack mapItem = new ItemStack(Material.MAP, 1, map.getId());
		ItemMeta mapMeta = mapItem.getItemMeta();

		mapMeta.setDisplayName("Marauder's Map");

		List<String> lore = new ArrayList<String>();
		lore.add("Scale: " + scale);
		mapMeta.setLore(lore);

		mapItem.setItemMeta(mapMeta);

		return mapItem;
	}
}
