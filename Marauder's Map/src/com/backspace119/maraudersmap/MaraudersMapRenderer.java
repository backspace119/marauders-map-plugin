package com.backspace119.maraudersmap;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.map.MapCanvas;
import org.bukkit.map.MapPalette;
import org.bukkit.map.MapRenderer;
import org.bukkit.map.MapView;

public class MaraudersMapRenderer extends MapRenderer
{
	public MaraudersMapRenderer()
	{
		super(true);
	}

	int lastX, lastZ;

	@Override
	public void render(MapView map, MapCanvas canvas, Player player)
	{
		Location location = player.getLocation();
		int playerX = location.getBlockX();
		int playerZ = location.getBlockZ();

		if(playerX == this.lastX && playerZ == this.lastZ)
		{
			return;
		}
		this.lastX = playerX;
		this.lastZ = playerZ;

		map.setCenterX(playerX);
		map.setCenterZ(playerZ);

		// update the map immediately
		player.sendMap(map);
	}
}
